var path = require('path');
var webpack = require("webpack");

module.exports = {
    entry: './js/app.js',
    output: {
        filename: 'app.js',
        path: path.resolve(__dirname, 'dist')
    },
    devtool: 'source-map'
};