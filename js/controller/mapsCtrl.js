var Point = require('../models/Point');
var placeTypes = require('../dicts/placeTypes');
var DB = require('../models/DB');
var map = require('../models/Map');
var pointFilter = require('../models/pointFilter');

module.exports = function($scope, $firebaseObject){
    $scope.points = app.points;
    $scope.newPoint = new Point();
    $scope.showAddOverlay = false;
    $scope.showFilterOverlay = false;
    $scope.filter = {
        name: '',
        description: '',
        categories: {},
        hasPhoto: false
    };
    $scope.addNewPoint = function(){
        var mapCenter = map.getMap().getCenter();
        $scope.showAddOverlay = true;
        $scope.newPoint.addBasePointToMap(mapCenter.lat(), mapCenter.lng());
    };
    $scope.placeTypes = placeTypes;
    $scope.submitNewPoint = function(){
        var newPoint = $scope.newPoint;
        var pointPosition = newPoint.mapPointLink.getPosition();
        if ((newPoint.name === '') || (newPoint.type === '')){
            return false;
        }
        $scope.points.push(newPoint);
        newPoint.lat = pointPosition.lat();
        newPoint.lng = pointPosition.lng();
        newPoint.addPointToMap();
        DB.savePoints($scope.points);
        $scope.newPoint = new Point();
        $scope.showAddOverlay = false;
    };
    $scope.cancelAddingPoint = function(){
        $scope.showAddOverlay = false;
        $scope.newPoint.mapPointLink.setMap(null);
        $scope.newPoint.mapPointLink = null;
    };
    $scope.$watch('filter', function(){
        var pointsFiltered = pointFilter($scope.points, $scope.filter);
        for (var i = 0; i < $scope.points.length; i++){
            if (pointsFiltered.indexOf($scope.points[i]) !== -1){
                $scope.points[i].showPoint();
            } else{
                $scope.points[i].hidePoint();
            }
        }
    }, true);
};