var firebase = require('firebase');

firebase.initializeApp({
    apiKey: "AIzaSyDq0YcmyUXruS6PKdzXctLc0QiY6UpZTqc",
    authDomain: "simplyinfo-dev.firebaseapp.com",
    databaseURL: "https://simplyinfo-dev.firebaseio.com",
    storageBucket: "simplyinfo-dev.appspot.com",
    messagingSenderId: "60928793927"
});
var dbRef = firebase.database().ref();
var DB = {
    getValue: function(callback){
        dbRef.once('value').then(function(snapshot) {
            callback(snapshot.val());
        });
    },
    setValue: function(data){
        dbRef.set(data);
    },
    savePoints: function(points){
        var pointsRaw = [];
        for (var i = 0; i < points.length ; i++){
            pointsRaw[i] = {};
            pointsRaw[i].lat = points[i].lat;
            pointsRaw[i].lng = points[i].lng;
            pointsRaw[i].name = points[i].name;
            pointsRaw[i].type = points[i].type;
            pointsRaw[i].description = points[i].description;
            pointsRaw[i].photo = points[i].photo;
        }
        this.setValue({points: pointsRaw});
    }
};
module.exports = DB;