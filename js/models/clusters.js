var Cluster = require('node-js-marker-clusterer');
var placeTypes = require('../dicts/placeTypes');
var map = require('./Map');

var clusters = {};
var isCreated = false;

module.exports = {
    getClusters: function(){
        if (!isCreated){
            for (var type in placeTypes){
                clusters[type] = new Cluster(map.getMap(), [], {
                    styles: [{
                        url: '/images/icons/'+type+'.png',
                        height: 37,
                        width: 32
                    }]
                });
            }
            isCreated = true;
        }
        return clusters;
    }
};
