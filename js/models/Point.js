var map = require('./Map');
var clusters = require('./clusters');
var DB = require('./DB');
var balloonTpl = require('../templates/balloonTpl');

var Point = function(data){
    if (data !== undefined){
        this.name = data.name || '';
        this.type = data.type || '';
        this.description = data.description || '';
        this.photo = data.photo || '';
        this.lat = data.lat || 0;
        this.lng = data.lng || 0;
    } else{
        this.name = '';
        this.type = '';
        this.description = '';
        this.photo = '';
        this.lat = 0;
        this.lng = 0;
    }
    this.isShown = false;
    this.mapPointLink = null;
};
Point.prototype = {
    addBasePointToMap: function(lat, lng){
        if (lat !== undefined){
            this.lat = lat;
        }
        if (lng !== undefined){
            this.lng = lng;
        }
        this.mapPointLink = new google.maps.Marker({
            map: map.getMap(),
            position: new google.maps.LatLng(this.lat, this.lng),
            icon: (this.type) ? '/images/icons/'+this.type+'.png' : null,
            draggable: true
        });
        this.isShown = true;
        return this;
    },
    addPointToMap: function(lat, lng){
        if (this.mapPointLink === null){
            this.addBasePointToMap(lat, lng);
        }
        if (this.icon !== ''){
            this.mapPointLink.setIcon('/images/icons/'+this.type+'.png');
        }
        var that = this;
        var balloon = new google.maps.InfoWindow({
            content: balloonTpl(that)
        });
        var latCache,
            lngCache;
        that.mapPointLink.addListener('click', function(){
            balloon.open(map.getMap(), that.mapPointLink);
        });
        if (that.type){
            clusters.getClusters()[that.type].addMarker(that.mapPointLink);
        }
        google.maps.event.addListener(that.mapPointLink, "dragstart", function(event){
            latCache = event.latLng.lat();
            lngCache = event.latLng.lng();
        });
        google.maps.event.addListener(that.mapPointLink, "dragend", function(event) {
            if (confirm('Подтвердите перенос точки')){
                that.lat = event.latLng.lat();
                that.lng = event.latLng.lng();
                DB.savePoints(app.points);
            } else{
                that.mapPointLink.setPosition(new google.maps.LatLng(latCache, lngCache));
            }
        });
        return this;
    },
    hidePoint: function(){
        if (this.isShown === true){
            clusters.getClusters()[this.type].removeMarker(this.mapPointLink);
            this.mapPointLink.setMap(null);
            this.isShown = false;
        }
    },
    showPoint: function(){
        if (this.isShown === false){
            this.mapPointLink.setMap(map.getMap());
            clusters.getClusters()[this.type].addMarker(this.mapPointLink);
            this.isShown = true;
        }
    }
};
module.exports = Point;