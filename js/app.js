var DB = require('./models/DB');
var Point = require('./models/Point');
var angular = require('angular');
require('angularfire');
var mapsCtrl = require('./controller/mapsCtrl');
var map = require('./models/Map');

app = {
    init: function(){
        var that = this;
        that.mapsModule = angular.module('mapsApp', ['firebase'])
            .controller('MapsCtrl', mapsCtrl);
        DB.getValue(function(data){
            var currentPoint;
            for (var i = 0; i < data.points.length; i++){
                currentPoint = new Point(data.points[i]);
                that.points.push(currentPoint);
                currentPoint.addPointToMap();
            }
        });
        that.points = [];
    }
};
app.init();