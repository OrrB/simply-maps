module.exports = function(point){
    return '<div class="balloon-content">' +
        ((point.photo) ?  '<img class="balloon-content-img" src="'+point.photo+'" />' : '') +
        '<div class="balloon-content-name">'+point.name+'</div>' +
        '<div class="balloon-content-text">'+point.description+'</div>' +
    '</div>';
}