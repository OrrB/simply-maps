function initMap() {
    app.map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 58.60567032964396, lng: 49.6747092666626},
        zoom: 14,
        mapTypeControl: false
    });
}